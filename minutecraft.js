/* Game Config */
const physics = { directions: [0.2, 0.6, 1.35], friction:[0.9999999, 0.5], turn: 0.005, gravity: 0.15, velocity: [0, 0, 0] }
const engine = { gameSize: [1048576, 1048576, 10], renderSize: 20, renderTimeout: 2000, chunkSize: 16, chunkInt: 1000, redrawInt: 1000, runInt: 20 }
const player = { coords: [0, 0, 0], angle: [0, 0], size:[0.9, 0.9, 1.9], actRange: 5, holding: 0 }
const colors =	[	false,	[0,0,0],	[128,128,128],	[60,200,60],	[200,200,60],	[60,120,200]	]
const frictions = [	0.99,	0.5,		0.5,			0.5,			0.5,			0.9				]
const terrains = [	false,	false,		15,				5,				2,				2				] // TODO: unify with colors & terrains?
const actions = [	{action: 'left',	velocity: [-1,  0,  0],	match: [{keyCode:37}, {keyCode:65}]},
					{action: 'right',	velocity: [ 1,  0,  0],	match: [{keyCode:39}, {keyCode:68}]},
					{action: 'forth',	velocity: [ 0,  1,  0],	match: [{keyCode:38}, {keyCode:87}]},
					{action: 'back',	velocity: [ 0, -1,  0],	match: [{keyCode:40}, {keyCode:83}]},
					{action: 'jump',	velocity: [ 0,  0,  1],	match: [{keyCode:32}], onceOnly: true },
					{action: 'dig',		match:	[{button:0}], onceOnly: true},
					{action: 'place',	match:	[{button:2}], onceOnly: true} ]
const fluids = [ 0, null ]
const colorFuncts = colors.map(color => distance => `rgb(${(multVS(color, 1 - ((0.7 * distance) / engine.renderSize)).join(','))})`)

/* Lookup Tables & Caches */
const	intervals = {},
		active = {}

/* Helper Utilities */
const	closestFirst = byKey('distance'),
		isSolid = coords => fluids.indexOf(nodeCache.getNode(coords.map(Math.round))) === -1,
		toString = n => n.toString(36),
		toInt = a => parseInt(a, 36)

/* Input Interpretation */
const findAction = ev => ({ match }) =>
	match.find(matcher => Object.keys(matcher).reduce((accum, key) => accum && matcher[key] === ev[key], true))
const mergeActive = (vl, a) => {
	if (a.velocity || (a.onceOnly === true && active[a.action] === 'done')) {
		if (a.onceOnly) active[a.action] = 'done'
		return { ...vl, vel: vl.vel.map((c, i) => c + (a.velocity[i] * physics.directions[i] * vl.fr)) }
	}
	return vl
}

/* Physics */
const collides = (coords, size = [1, 1, 1]) =>
	comb({ size: 2, c: coords, m: size }).find(isSolid) !== undefined
const getNearest = (coords, size = [1, 1, 1]) =>
	comb({ size: [2, 2, 1], c: coords, m: size })
		.map(a => ({ points: a, node: nodeCache.getNode(a.map(Math.round)) }))
		.filter(({ node }) => (fluids.indexOf(node) === -1))
		.map(({ points, node }) => ({ distance: thag(points.map(Math.round), coords), node }))
		.sort(closestFirst)[0]?.node
const move = direction => {
	const newPosition = addV(player.coords, direction)
	// TODO: collide any size cuboid by chunking into cubes?
	return collides(newPosition, multV(player.size, [1, 1, 0.5])) === false
		&& collides(addV(newPosition, [0, 0, -1]), multV(player.size, [1, 1, 0.5])) === false
}
const physicsNew = () => {
	// TODO: I broke something
	const actives = Object.values(active).filter(notFalsey)
	const below = getNearest(addV(player.coords, [0, 0, -(player.size[2] - 0.5)]))
	const floorFriction = below > 0 ? frictions[below] : frictions[0]
	const rotated = rotateI(actives.reduce(mergeActive, { fr: 1 - floorFriction, vel: [0, 0, 0] }).vel, [0, 0, 0], [player.angle[0], 0])
	physics.velocity = addV(physics.velocity, [0, 0, -physics.gravity]).map((c, i) => {
		const d = (c + rotated[i]) * ((i !== 2) ? floorFriction : frictions[0])
		const v = setV([0, 0, 0], i, d)
		return move(v) ? d : 0
	})
	player.coords = addV(player.coords, physics.velocity)
	if (player.coords[2] < -10)
		player.coords = addV(multV(nodeCache.bound(player.coords, 1), [1, 1, 0]), addV(multV(engine.gameSize, [0, 0, 1]), [0, 0, player.size[2]])) // if the player falls out of bounds, put them back in
}
const applyPhysics = () => {
	const [ actives, below ] = [ Object.values(active).filter(notFalsey), getNearest(addV(player.coords, [0, 0, -(player.size[2] - 0.5)])) ]
	const floorFriction = below > 0 ? frictions[below] : frictions[0]
	const rotated = rotateI(actives.reduce(mergeActive, { fr: 1 - floorFriction, vel:[0, 0, 0] }).vel, [0, 0, 0], [player.angle[0], 0])
	physics.velocity = addV(physics.velocity, [0, 0, -physics.gravity]).map((c, i) => {
		const [d, v] = [(c + rotated[i]) * ((i !== 2) ? floorFriction : frictions[0]), [0, 0, 0]]
		return (v[i] = d) && (move(v) ? ((player.coords = addV(player.coords, v)) && d) : 0)
	})
	if (player.coords[2] < -10)
		player.coords = addV(
			multV(nodeCache.bound(player.coords, 1), [1, 1, 0]),
			addV(multV(engine.gameSize, [0, 0, 1]), [0, 0, player.size[2]])
		) // if the player falls out of bounds, put them back in
}

/* World Editing */
const placeBlock = (coords, material = 0) => {
	const originalMaterial = nodeCache.getNode(coords)
	nodeCache.setNode(coords, material)
	if (fluids.indexOf(material) === -1 && !move([0, 0, 0]))
		return nodeCache.setNode(coords, originalMaterial)
	storage.addValue(coords, material)
	blockRenderer.reloadNeighbors(coords)
}
const makeBreak = () => {
	if (!((active.dig && active.dig !== 'done') || (active.place && active.place !== 'done'))) return

	if (active.dig) active.dig = 'done'
	if (active.place) active.place = 'done'
	const	rangeEnd = rotateI([0, player.actRange, 0], [0, 0, 0], player.angle),
			gradient = multVS(rangeEnd, 1 / player.actRange),
			signs = rangeEnd.map(Math.sign),
			bsd = [],
			bsp = [],
			_ =
	[].concat(...rangeEnd.map((c,j) =>
		Array(Math.floor(Math.abs(c))).fill(0)
		.map((c,i) => ({
			face: j,
			distance: (Math.round(player.coords[j] + (i * signs[j])) + (0.5 * signs[j]) - player.coords[j]) / gradient[j]
		}))
	))
	.filter(a => a.distance >= 0 && a.distance <= player.actRange)
	.sort(closestFirst)
	.forEach(factor => {
		const pn = addV(player.coords, multVS(gradient, factor.distance))
		const d = setV([0, 0, 0], factor.face, 0.5 * signs[factor.face])
		bsd.push(addV(pn, d).map(Math.round))
		if (active.place) bsp.push(addV(pn, multVS(d, -1)).map(Math.round))
	})
	const delI = bsd.findIndex(isSolid)
	if (delI === -1) return
	if (active.dig)
		placeBlock(bsd[delI], 0)
	else if (active.place && player.holding && bsp[delI][2] !== (engine.gameSize[2] - 1))
		placeBlock(bsp[delI], player.holding)
}

/* Run Loops */
let running = false
const run = () => {
	running = setTimeout(run, engine.runInt)
	applyPhysics()
	makeBreak()
	
	if (blockRenderer.isMode(3)) {
		blockRenderer.addCustomBlock('player', addV(player.coords, [0, 0, -0.5]), player.size, '#ff8855')
	} else blockRenderer.removeCustomBlock('player')

	if (player.holding) {
		blockRenderer.addCustomBlock('holding', addV(player.coords, rotateI([Math.min(0.5, blockRenderer.getSize()[0] / 2000), 1, -0.2], [0, 0, 0], player.angle)), [0.2, 0.2, 0.2], player.holding)
	} else blockRenderer.removeCustomBlock('holding')
}
const start = () => {
	intervals.chunks = setInterval(() => nodeCache.loadChunksNear(player.coords, engine.renderSize * 2), engine.chunkInt)
	intervals.view = setInterval(() => blockRenderer.reloadView(player.coords, engine.renderSize, engine.renderTimeout), engine.redrawInt)
	running = setTimeout(run, engine.runInt)
	blockRenderer.start()
}
const stop = () => {
	clearTimeout(running)
	running = false
	blockRenderer.stop()
	Object.values(intervals).forEach(clearInterval)
}

/* Events */
const lockPointerMouseMove = (element, callback) => {
	const lockChangeAlert = () => {
		if (document.pointerLockElement === element || document.mozPointerLockElement === element)
			document.addEventListener("mousemove", callback, false)
		else document.removeEventListener("mousemove", callback, false)
	}
	const bindAnyway = () => document.addEventListener("mousemove", callback, false)
	element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock
	document.exitPointerLock = document.exitPointerLock || document.mozExitPointerLock
	element.onclick = () => element.requestPointerLock()
	document.addEventListener('pointerlockerror', bindAnyway, false)
	document.addEventListener('mozpointerlockerror', bindAnyway, false)
	document.addEventListener('pointerlockchange', lockChangeAlert, false)
	document.addEventListener('mozpointerlockchange', lockChangeAlert, false)
}
const evDown = event => { // register action as active on press
	const action = actions.find(findAction(event))
	if (action && !active[action.action]) active[action.action] = action
}
const evUp = event => { // unregister action as active on release
	const action = actions.find(findAction(event))
	if (action) active[action.action] = false
}
const lookArround = event => { // Look around with the cursor
	player.angle[0] -= event.movementX * physics.turn
	player.angle[1] += event.movementY * physics.turn
}
const chooseItem = event => { // Number keys to choose item
	const ki = parseInt(event.key)
	if (event.key == ki)
		player.holding = ki < colors.length ? ki : 0
}
const cancelEvent = event => { // Cancel any default event
	if(event.preventDefault != undefined) event.preventDefault()
	if(event.stopPropagation != undefined) event.stopPropagation()
	return false
}
const canvasSize = event => // Update canvas size
	blockRenderer.setSize([ window.innerWidth, window.innerHeight ])

/* Seed Based RNG */
const urlParams = new URLSearchParams(window.location.search)
const seed = urlParams.has('seed') ? parseFloat(urlParams.get('seed')) : Math.random()
const random = randomGenerator(seed)

/* Stored Data */
function BlockStorage(storageKey) {
	const indexer = coords => coords.map(toString).join(',')
	const storedBlocks = {}

	// Init storage
	if (localStorage[storageKey] === undefined) localStorage[storageKey] = ""
	
	// Read storage string into storage object
	localStorage[storageKey]
		.split('|')
		.filter(notFalsey)
		.map(str => str.split(',').map(a => parseInt(a)))
		.forEach(([ material, ...coords ]) => storedBlocks[indexer(coords)] = material)

	// Re-save storage object to storage string
	localStorage[storageKey] = "|"+
		(Object.entries(storedBlocks)
			.map(([ index, material ]) => ([ material, ...index.split(',').map(toInt) ].join(',')))
			.join('|'))

	// Interface
	const hasValue = coords => storedBlocks[indexer(coords)] !== undefined
	const addValue = (coords, material)  => {
		const matInt = parseInt(material)
		const index = indexer(coords)
		localStorage[storageKey] = localStorage[storageKey] + "|" + ([matInt, ...coords].join(','))
		storedBlocks[index] = matInt
	}
	const getValue = coords => storedBlocks[indexer(coords)]

	this.addValue = addValue
	this.getValue = getValue
	this.hasValue = hasValue
}

/* World Generation */
const worldGeneration = random => {
	const newFourierTerm = (funct, [m = 1, n = 1, c = 0, d = 0]) => x => c + (m * funct(n * (x + d)))
	const newFourierSeries = terms => x => terms.reduce((acc, term) => acc + term(x), 0)
	const newFourierConfig = terms => ['x', 'y'].map(() =>
		Array(terms).fill(0).map(() =>
			[random(), random(), (random() - 0.5) * 2, (random() - 0.5) * 2]
		))
	const newFourierFunction = cnf => newFourierSeries(cnf.map(n => newFourierTerm(Math.sin, n)))
	const terrainFunctions = terrains.map(c => {
		if (!c) return () => 0
		const D2conf = newFourierConfig(c).map(newFourierFunction)
		return ([x, y]) => D2conf[0](x) + D2conf[1](y)
	})
	return (coords, index) => Math.max(terrainFunctions.findIndex(f => f(coords) > coords[2]), 0)
}

const storage = new BlockStorage(`minutecraft-custom-${seed}`)
const generator = worldGeneration(random)
const nodeCache = chunkLoader({
	fieldRange: engine.gameSize,
	chunkSize: engine.chunkSize,
	indexer: coords => coords.map(toString).join(','),
	generator: (coords, index) => {
		if (storage.hasValue(coords))
			return storage.getValue(coords) // Edited blocks
		if (coords[2] === 0)
			return 1 // Bedrock base
		if (coords[2] === (engine.gameSize[2] - 1))
			return 0 // Air sky (for rendering purposes)
		return generator(coords, index)
	}
})

/* Init */
player.coords = addV(multV(engine.gameSize, [0.5, 0.5, 1]), [0, 0, player.size[2]])
document.write(`<i style="text-align:center;width:100%;height:100px;line-height:100px;position:absolute;top:0;bottom:0;left:0;margin:auto;">loading...</i>`)
Promise.all(nodeCache.loadChunksNear(player.coords).map(l => l.promise()))
.then(() => new BlockRenderer(document.createElement('canvas'), nodeCache)
				.reloadView(player.coords, engine.renderSize, engine.renderTimeout))
.then(blockRenderer => {
	window.blockRenderer = blockRenderer
	document.body.innerHTML = "<style>*{padding:0;margin:0;background:#8af;}</style>"
	document.body.appendChild(blockRenderer.getCanvas())
	lockPointerMouseMove(blockRenderer.getCanvas(), lookArround)
	canvasSize()
	window.addEventListener("resize", canvasSize)
	document.addEventListener("keydown", evDown)
	document.addEventListener("keyup", evUp)
	document.addEventListener("mousedown", evDown)
	document.addEventListener("mouseup", evUp)
	document.addEventListener("contextmenu", cancelEvent)
	document.addEventListener("keydown", chooseItem)
	start()
})
