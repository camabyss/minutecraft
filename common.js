Math.TAU = Math.PI * 2
const boundAngle = a => { // Keeps an angle between 0 and 2PI
	if (a < 0) return Math.TAU - (Math.abs(a) % Math.TAU)
	return a % Math.TAU
}

/* Vector Library */
const	thag = (a, b = [0, 0, 0]) => Math.sqrt(a.reduce((sum, c1, i) => (sum + ((b[i] - c1) ** 2)), 0)),
		multV = (a,b) => a.map((c,i) => b[i] * c),
		divV = (a,b) => a.map((c,i) => c / b[i]),
		addV = (a,b) => a.map((c,i) => b[i] + c),
		negV = (a,b) => a.map((c,i) => c - b[i]),
		multVS = (a,b) => a.map(c => b * c),
		divVS = (a,b) => a.map(c => c / b),
		addVS = (a,b) => a.map(c => b + c),
		negVS = (a,b) => a.map(c => c - b),
		eqV = (a,b) => a.findIndex((c,i) => c !== b[i]) === -1,
		eqVS = (a,b) => a.findIndex((c,i) => c !== b) === -1,
		setV = (a,index,b) => a.map((c,i) => i === index ? b : c)

/* Functional Helpers */
const	notFalsey = a => a,
		isUndefined = a => a === undefined,
		byKey = key => (a, b) =>  a[key] - b[key],
		byKeyRev = key => (a, b) => b[key] - a[key],
		mult = (a, b) => a * b

/* 3D Library */
const rotate = (coords, center, angles) => {
	// rotate 3D coords by the angles about the center
	const	dx = coords[0] - center[0],
			dy = coords[1] - center[1],
			dz = coords[2] - center[2],
			sinA = Math.sin(angles[0]),
			cosA = Math.cos(angles[0]),
			sinB = Math.sin(angles[1]),
			cosB = Math.cos(angles[1])
	const y2 = dy * cosA - dx * sinA
	return	[
		center[0] + (dx * cosA + dy * sinA),
		center[1] + (y2 * cosB - dz * sinB),
		center[2] + (dz * cosB + y2 * sinB)
	]
}
const rotateI = (coords, center, angle) => rotate(rotate(coords, center, [0, -angle[1]]), center, [-angle[0], 0])
// const project = ({ // defined in renderer
// 	coords	= [0, 0, 0],
// 	angle	= [0, 0],
// 	perspective = 1000,
// 	size	= [100, 100]
// }, pointCoords) => { // projects 3D coords into 2D coords given a view position & angle
// 	const rotated = rotate(pointCoords, coords, angle)
// 	const perspectiveDistance = perspective / (rotated[1] - coords[1])
// 	if (perspectiveDistance < 0) return false
// 	return [
// 		 ((rotated[0] - coords[0]) * perspectiveDistance) + (size[0] / 2),
// 		-((rotated[2] - coords[2]) * perspectiveDistance) + (size[1] / 2)
// 	]
// }

/* Combinatorics */
function comb({ d = 3, s = 2, m = 1, c = 0, centered = true }) {
	// List all combinations of numbers in D dimensions, and of S size
	const	ss = s instanceof Array ? s : Array(d).fill(s),
			ms = m instanceof Array ? m : Array(d).fill(m),
			cs = c instanceof Array ? c : Array(d).fill(c)
	// TODO: this is a little messy, maybe a normal for loop is better
	return Array(ss.reduce(mult, 1)).fill(c)
	.map((J, b) => Array(d).fill(c).reduce(({ b, r }, c, i) => {
		const csi = cs[i] - (centered ? ms[i] / 2 : 0)
		if (b === 0) return { b, r: r.concat(csi) }
		const mag = ss.slice(i + 1).reduce(mult, 1)
		if (mag > b) return { b, r: r.concat(csi) }
		const e = Math.floor(b / mag)
		return {
			b: b - (e * mag),
			r: r.concat([ csi + (ms[i] * e) ])
		}
	}, { b, r: [] }).r)
}

/* Async Process "Thread" */
function Process(processor, initial = [], config = {}) {
	let queue = []
	const { take = 1, delay = 0, autostart = false, selfInit = false } = config
	let		paused = false,
			started = false,
			concluded = false,
			complete = false,
			failed = false,
			terminated = false,
			activeDelay = false,
			resolve = false,
			reject = false
	const promise = new Promise((res, rej) => {
		resolve = res
		reject = rej
	})
	if (selfInit && queue.length === 0) queue.push('INIT PROCESS')
	const statistics = { processed: 0, remaining: queue.length, duration: 0, start: undefined, end: undefined }
	const reserved = Object.keys(statistics)
	const extend = (...args) => {
		for (const a in args) queue.push(args[a])
		statistics.remaining += args.length
		return extend
	}
	const report = reportObj => {
		for (const r in reportObj)
			if (reserved.indexOf(r) === -1)
				statistics[r] = reportObj[r]
			else throw new Error(`You cannot report on reserved statistics`)
		return report
	}
	const step = data => processor(data, { extend, report, statistics:{ ...statistics }})
	const stepWise = () => {
		if (paused) return
		const t1 = Date.now()
		const taken = queue.splice(0,take)
		try {
			for (const t in taken) {
				step(taken[t])
				statistics.processed += 1
				statistics.remaining -= 1
			}
		} catch(e) {
			failed = e
			this.terminate(e)
		}
		if (concluded) return
		const duration = Date.now()-t1
		statistics.duration += duration
		if (queue.length)
			activeDelay = setTimeout(stepWise, delay+duration)
		else {
			complete = true
			concluded = true
			activeDelay = false
			statistics.end = Date.now()
			resolve(statistics)
		}
	}
	this.started = () => started
	this.paused = () => paused
	this.completed = () => complete
	this.concluded = () => concluded
	this.failed = () => failed
	this.terminated = () => terminated

	this.statistics = function (req = false) {
		if (req === false) return { ...statistics }
		if (!(req instanceof Array)) req = [req]
		const ret = {}
		for (const r in req)
			ret[req[r]] = typeof r === 'string' ? statistics[r] : undefined
		return ret
	}
	this.terminate = function (reason) {
		if (concluded) throw new Error('Process cannot be terminated once it is concluded')
		concluded = true
		clearTimeout(activeDelay)
		activeDelay = false
		statistics.end = Date.now()
		terminated = reason
		reject(reason)
		return this
	}
	this.pause = function () {
		if (concluded) throw new Error('Process cannot be paused once it is concluded')
		if (!activeDelay) throw new Error('Process is already paused and cannot be paused again')
		clearTimeout(activeDelay)
		paused = true
		activeDelay = false
		return this
	}
	this.start = function () {
		if (concluded) throw new Error('Process cannot be restarted once it is concluded')
		if (activeDelay) throw new Error('Process is already active and cannot be restarted')
		if (!started) {
			statistics.start = Date.now()
			queue = [].concat(initial)
			statistics.remaining = queue.length
			started = true
		}
		paused = false
		activeDelay = setTimeout(stepWise, delay)
		return this
	}
	this.promise = function () {
		return promise
	}
	if (autostart) {
		this.start()
	}
}

/* Data Cache */
function chunkLoader ({
	chunkSize	= 32,
	loadMarker	= undefined,
	rangeMarker	= null,
	processConfig = { take: 1000, delay: 0, autostart: true },
	fieldRange	= [ Number.MAX_SAFE_INTEGER + 1, Number.MAX_SAFE_INTEGER + 1, Number.MAX_SAFE_INTEGER + 1 ],
	generator	= coords => coords[coords.length - 1] === 0 ? 1 : 0,
	indexer		= coords => coords.join(',')
}) {
	const	loadedData = {},
			loadedChunks = [],
			unloadedChunk = ({ coords }) => loadedChunks.indexOf(indexer(coords)) === -1
	const loadNode = (pcoords, index) => {
		if (loadedData[index] !== undefined) return false
		loadedData[index] = generator(pcoords, index)
	}
	const loadChunk = ({ coords }) => {
		loadedChunks.push(indexer(coords))
		return forRangeProcess(multV(coords, chunkSizes), chunkSizes, loadNode)
	}
	const inrangeF = range => {
		const isOutOfRange = (c, i) => c < 0 || c >= range[i]
		return coords => !coords.find(isOutOfRange)
	}
	// TODO: what is offset? why do I need it? remove it so this can be faster
	const boundF = range => (coords, offset = 0) => coords.map((c, i) => c < 0 ? 0 : (c > (range[i] - offset) ? (range[i] - offset) : c))
	const loadChunksNear = (position, loadRange = thag(chunkSizes)) =>
		comb({ d: fieldRange.length, s: 3, centered: false, c: -1 })
			.map(c => ({
				coords: addV(chunkBound(divV(position, chunkSizes).map(Math.floor), 1), c),
				distance: thag(multV(addV(chunkBound(divV(position, chunkSizes).map(Math.floor), 1), addVS(c, 0.5)), chunkSizes), position)
			}))
			.filter(({ distance, coords }) => (distance < loadRange) && chunkInrange(coords))
// 			.sort(closestFirst)
			.filter(unloadedChunk)
			.map(loadChunk)
	const setNode = (coords, value) => {
		if (inrange(coords)) loadedData[indexer(coords)] = value
	}
	const bound = boundF(fieldRange)
	const inrange = inrangeF(fieldRange)
	const chunkSizes = bound(chunkSize instanceof Array ? chunkSize : Array(fieldRange.length).fill(chunkSize))
	const chunkBound = boundF(divV(fieldRange, chunkSizes).map(Math.floor))
	const chunkInrange = inrangeF(divV(fieldRange, chunkSizes).map(Math.floor))
	const getNode = coords => { // get node at coords right now, or falsey
		if (!inrange(coords)) return rangeMarker
		const index = indexer(coords)
		if (loadedData[index] !== undefined) return loadedData[index]
		return loadMarker
	}
	const forRangeProcess = (USloadCoords, USloadSize, funct, processOpts = processConfig) => {
		const loadCoords = bound(USloadCoords) // US = UnSafe
		const loadSize = bound(USloadSize)
		const forIndexToCoords = index => loadSize.map((c, i) => Math.floor(index / (Array(i).fill(0).map((a, j) => loadSize[j]).reduce(mult, 1))) % c) // TODO: this could be re-written to load closest first?
		return new Process((item, { statistics }) => {
			const coords = addV(forIndexToCoords(statistics.processed), loadCoords)
			funct(coords, inrange(coords) ? indexer(coords) : -1)
		}, Array(loadSize.reduce(mult, 1)).fill(loadMarker), processOpts) // TODO: avoid filling array?
	}
	return { bound, getNode, setNode, getIndex: indexer, loadChunksNear, forRangeProcess }
}

/* Drawing */
const drawPolygon = (context, points) => { // draws a closed polygon
	if (points.length < 3) return // polygons must have at least 3 points
	const [first, ...otherPoints] = points
	context.beginPath()
	context.moveTo(first[0], first[1])
	otherPoints.forEach(point => context.lineTo(point[0], point[1]))
	context.closePath()
	context.fill()
	context.stroke()
}

const randomGenerator = seed => () => Math.abs((Math.log(seed++) * 10000) % 1)
