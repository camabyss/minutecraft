const closestLast = byKeyRev('distance')
/* Renderer should:
 * render polygons
 * filter polygons before rendering
 * hold information about the 3D view position
 * NOT be specific to cubes
 */
function Renderer(canvas) {
	const	context		= canvas.getContext('2d'),
			size		= [ canvas.width, canvas.height ],
			coords		= [ 0, 0, 0 ],
			angle		= [ 0, 0 ],
			angleLimit	= [ false, Math.PI / 2 ]
	let		perspective	= 1000,
			mode		= 1
	let polygons = []
// 	const projectPoint = c => project({ coords, angle, perspective, size}, c)
	const projectPoint = pointCoords => { // projects 3D coords into 2D coords given a view position & angle
		const rotated = rotate(pointCoords, coords, angle)
		const perspectiveDistance = perspective / (rotated[1] - coords[1])
		if (perspectiveDistance < 0)
			return false
		return [
			((rotated[0] - coords[0]) * perspectiveDistance) + (size[0] / 2),
			-((rotated[2] - coords[2]) * perspectiveDistance) + (size[1] / 2)
		]
	}
	
	const drawFace = ({ points, color }) => {
		context.fillStyle = context.strokeStyle = color
		drawPolygon(context, points)
	}
	const getFace = ({ face, distance }) => {
		const points = face.coords.map(projectPoint).filter(notFalsey)
		if (points.length < 3)
			return false
		const bounds = getBounds(points)
		return (bounds[0][1] < 0 || bounds[1][1] < 0 || bounds[0][0] > size[0] || bounds[1][0] > size[1])
			? false
			: { points, color: face.color instanceof Function ? face.color(distance) : face.color }
	}
	const getBounds = polygon => [0, 1].map(xy => {
		const c = polygon.map(p => p[xy])
		return [ Math.min(...c), Math.max(...c) ]
	})
	const getDistances = face => ({ face, distance: thag(coords, face.center) })
	
	const limitAngle = (a, l) => {
		if (l === false)
			return a
		if (a > l)
			return l
		if (a < -l)
			return -l
		return a
	}
	const render		= polygons => {
		const renderPolygons = polygons
			.map(getDistances)
			.sort(closestLast)
			.map(getFace)
			.filter(notFalsey)
			.slice(-1000)
		context.clearRect(0, 0, size[0], size[1])
		renderPolygons.forEach(drawFace)
	}
	const moveAngle		= addAngle => setAngle([ angle[0] + addAngle[0], angle[1] + addAngle[1] ])
	const setAngle		= newAngle => {
		angle[0] = boundAngle(limitAngle(newAngle[0], angleLimit[0]))
		angle[1] = boundAngle(limitAngle(newAngle[1], angleLimit[1]))
	}
	const setPosition	= newPosition => {
		coords[0] = newPosition[0]
		coords[1] = newPosition[1]
		coords[2] = newPosition[2]
	}
	const setSize		= newSize => {
		if (!(newSize instanceof Array))
			throw new Error("The argument passed to setSize must be an Array.")
		if (newSize.length !== 2)
			throw new Error("The argument passed to setSize must be a 2D Array, it is the width and height of the canvas.")

		const integers = newSize.map(n => parseInt(n))
		if (integers.find(isNaN) !== undefined)
			throw new Error("One of the dimensions passed to setSize is not a number.")

		size[0] = canvas.width = integers[0]
		size[1] = canvas.height = integers[1]
	}
	const getSize		= () => [...size]
	
	const setMode		= newMode => {
		const integer = parseInt(newMode)
		if (isNaN(integer))
			throw new Error("View mode must be an integer.")
		if (integer < 1 || integer > 3)
			throw new Error("View mode must be 1, 2 or 3.")
		mode = integer
	}
	const getMode		= () => mode
	const isMode		= checkMode => parseInt(checkMode) === mode

	const setPerspective= newPerspective => {
		const floater = parseFloat(newPerspective)
		if (isNaN(floater))
			throw new Error("View perspective must be a number.")
		if (floater < 0)
			throw new Error("View perspective must be positive.")
		perspective = floater
	}
	const setAngleLimit	= newLimit => {
		if (!(newLimit instanceof Array))
			throw new Error("The argument passed to setAngleLimit must be an Array.")
		if (newLimit.length !== 2)
			throw new Error("The argument passed to setAngleLimit must be a 2D Array.")
		const floaters = newLimit.map(lim => (lim === false) ? lim : parseFloat(lim))
		if (floaters.find(lim => lim !== false || isNaN(lim)) !== undefined)
			throw new Error("An angle limit must be a number, or false for no limit")
		angleLimit[0] = floaters[0]
		angleLimit[1] = floaters[1]
	}

	this.moveAngle = moveAngle
	this.setAngle = setAngle
	this.setAngleLimit = setAngleLimit
	this.setPerspective = setPerspective
	this.setMode = setMode
	this.getMode = getMode
	this.isMode = isMode
	this.setSize = setSize
	this.getSize = getSize
	this.setPosition = setPosition
	this.render = render
}

function BlockRenderer(canvas, nodeCache) {
	// TODO: this is still not decoupled
	/* Cube Renderer Should:
	 * render cubes
	 * calculate polygons from cubes
	*/
	const _this = this
	const polygonRenderer = new Renderer(canvas)


	const neighbors = (center, size = [1, 1, 1]) =>
		[].concat(...[0, 1, 2].map(i => [1, -1].map(m => [ setV(center, i, center[i] + (size[i] / 2 * m)), m, i ])))

	const reloadNeighbors = coords =>
		[coordsAndIndex(coords.map(Math.round))]
		.concat(neighbors(coords, [2, 2, 2])
			.map(roundFirst)
			.map(coordsAndIndex)
			.filter(needsRefresh))
		.forEach(refreshNode)


	const refreshNode = ([c,i]) => {
		if (knownFaces[i] !== undefined) delete knownFaces[i]
		if (viewRange[i] !== undefined) delete viewRange[i]
		reloadNode(c, i)
	}
	const needsRefresh = ([c,i]) => (knownFaces[i] !== undefined) || (viewRange[i] !== undefined)

	// TODO: fix this, it's inaccurate anyway, should be based on viewer, should not have to implement boundAngle unless inside renderer
	const behindYou = ({ coords: c }) => boundAngle(Math.atan2(c[1] - player.coords[1], c[0] - player.coords[0]) - player.angle[0]) < Math.PI
	
	const coordsAndIndex = coords => [coords, nodeCache.getIndex(coords)]

			
	const	viewRange = {},
			knownFaces = {},
			customPolygons = {}
			

	const drawAll = () => {
		if (polygonRenderer.isMode(3)) {
			polygonRenderer.setPosition(addV(player.coords, rotateI([0, 10, 0], [0, 0, 0], player.angle)))
			polygonRenderer.setAngle([ player.angle[0] + Math.PI, -player.angle[1] ])
		} else {
			polygonRenderer.setPosition(player.coords)
			polygonRenderer.setAngle(player.angle)
		}
		polygonRenderer.setPosition(player.coords)
		polygonRenderer.setAngle(player.angle)
		const worldPolygons = Object.values(viewRange)
			.filter(behindYou)
			.map(filterBlockPolygons)
		
		polygonRenderer.render([].concat(...worldPolygons, ...Object.values(customPolygons)))
		if (animating) window.requestAnimationFrame(drawAll)
	}

	const filterBlockPolygons = ({ faces, material }) =>
		faces.map(face => ({
			...face, 
			color: colorFuncts[material] || material,
		}))
	const	facesOrder = ([a, b, c, d]) => [a, c, d, b],
			roundFirst = c => c[0].map(Math.round)
	const getCubeNeighbors = coords =>
		neighbors(coords, [2, 2, 2])
			.map(roundFirst)
			.map(nodeCache.getNode)
	const getCubeFaces = (center, size = [1, 1, 1]) =>
		neighbors(center, size)
		.map(([cent, m, i]) => ({
			center: cent,
			coords: facesOrder(comb({
				s: setV([2, 2, 2], i, 1),
				c: setV(center, i, m === 1 ? center[i] + size[i] : center[i]),
				m: size
			}))
		}))
	const getFaces = (coords,index) => {
		const knowNode = knownFaces[index]
		if (knowNode === false || knowNode instanceof Array) return knowNode
		const neighbors = getCubeNeighbors(coords)
		const pre = neighbors.find(isUndefined)
		const faces = knowNode instanceof Object ? knowNode.faces : getCubeFaces(coords)
		const result = faces.filter((p, i) => neighbors[i] === 0 || isUndefined(neighbors[i]))
		const response = result.length ? result : false
		if (! pre) knownFaces[index] = response
		else if (!(knowNode instanceof Object)) knownFaces[index] = { faces }
		return response
	}

	const reloadNode = (coords, index) => {
		if (viewRange[index] !== undefined) {
			viewRange[index].time = Date.now()
			return
		}
		const material = nodeCache.getNode(coords)
		if (material < 1 || isUndefined(material)) return
		const faces = getFaces(coords, index)
		if (faces) viewRange[index] = { material, coords, faces, time: Date.now() }
	}
	const reloadView = (coords, size, timeout) => nodeCache.forRangeProcess(
			negVS(coords, size).map(Math.round),
			Array(3).fill(size * 2).map(Math.round),
			reloadNode
		).promise().then(() => {
			Object.keys(viewRange)
				.slice(0, 2000)
				.filter(key => (Date.now() - viewRange[key].time) > timeout)
				.forEach(key => delete viewRange[key])
			return _this
		})

	const addCustomBlock = (name, position, size, color) => {
		customPolygons[name] = filterBlockPolygons({
			material: color,
			faces: getCubeFaces(position, size)
		})
	}
	const removeCustomBlock = name => {
		if (customPolygons[name] !== undefined)
			delete customPolygons[name]
	}

	let animating = false
	const start = () => {
		animating = true
		window.requestAnimationFrame(drawAll)
	}
	const stop = () => {
		animating = false
	}




	this.addCustomBlock = addCustomBlock
	this.removeCustomBlock = removeCustomBlock
	this.reloadNeighbors = reloadNeighbors
	this.reloadView = reloadView
	this.start = start
	this.stop = stop
	this.getCanvas = () => canvas
	
	this.setSize = polygonRenderer.setSize
	this.getSize = polygonRenderer.getSize
	this.isMode = polygonRenderer.isMode
}
