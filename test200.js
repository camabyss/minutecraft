const MS = { directions: [0.2,0.6,1.35], friction:[0.9999999,0.5], turn: 0.005, gravity: 0.15, velocity: [0,0,0] }
const LD = { gameSize: [1048576, 1048576, 10], renderSize: 20, renderTimeout: 2000, chunkSize: 16, chunkInt:1000, redrawInt: 1000, runInt: 20 }
const PL = { coords: [0,0,0], angle: [0,0], size:[0.9,0.9,1.9], actRange: 5, holding: 0 }
const VP = { size: [0,0], perspective: 1000, coords: [0, 0, 0], angle: [0,0], mode: 1, limit: (Math.PI / 2) - 0.4, borders: false }
const colors =	[	false,	[0,0,0],	[128,128,128],	[60,200,60],	[200,200,60],	[60,120,200]	]
const frictions = [	0.99,	0.5,		0.5,			0.5,			0.5,			0.9				]
const terrains = [	false,	false,		15,				5,				2,				2				] // TODO: unify with colors & terrains?
const actions = [	{action:'left',  velocity:[-1,  0,  0], match:[{keyCode:37}, {keyCode:65}]},
					{action:'right', velocity:[ 1,  0,  0], match:[{keyCode:39}, {keyCode:68}]},
					{action:'forth', velocity:[ 0,  1,  0], match:[{keyCode:38}, {keyCode:87}]},
					{action:'back',  velocity:[ 0, -1,  0], match:[{keyCode:40}, {keyCode:83}]},
					{action:'jump',  velocity: [ 0,  0, 1], match:[{keyCode:32}], onceOnly: true },
					{action:'dig',   match:[{button:0}], onceOnly: true}, {action:'place', match:[{button:2}], onceOnly: true} ]
const getColor = (material, distance) => material in colors ? `rgb(${(multVS(colors[material], 1 - ((0.7*distance)/LD.renderSize)).join(','))})` : material
/* Lookup Tables & Caches */ const [ knownFaces, storedBlocks, viewRange, customPolygons, intervals, active, fluids ] = [ {}, {}, {}, {}, {}, {}, [0, null] ]
/* Vector Library */         const [ thag, multV, divV, addV, negV, multVS, divVS, addVS, negVS, eqV, eqVS, setV ] = [ (a, b) => Math.sqrt(a.reduce((sum, c1, i) => (sum + ((b[i] - c1) ** 2)), 0)), (a,b) => a.map((c,i) => b[i]*c), (a,b) => a.map((c,i) => c/b[i]), (a,b) => a.map((c,i) => b[i]+c), (a,b) => a.map((c,i) => c-b[i]), (a,b) => a.map(c => b*c), (a,b) => a.map(c => c/b), (a,b) => a.map(c => b+c), (a,b) => a.map(c => c-b), (a,b) => a.findIndex((c,i) => c !== b[i]) === -1, (a,b) => a.findIndex((c,i) => c !== b) === -1, (a,index,b) => a.map((c,i) => i===index?b:c) ]
/* Helper Utilities */       const [ notFalsey, isUndefined, byDistance, facesOrder, isSolid ] = [ a => a, a => a === undefined, (a, b) => b.distance - a.distance, ([a,b,c,d]) => [a,c,d,b], coords => fluids.indexOf(NC.getNode(coords.map(Math.round))) === -1 ]
/* Too Many One-Liners*/     const [ collides, getNearest, getCubeNeighbors, randomGenerator, findAction, mergeActive, behindYou, boundAngle, filterBlockPolygons, reloadView, canvasSize, getCubeFaces, neighbors ] = [ (coords, size = [1,1,1]) => comb({ size: 2, c:coords, m: size }).find(isSolid) !== undefined, (coords, size = [1,1,1]) => comb({ size: [2,2,1], c:coords, m: size }).map(a => ({ points:a, node: NC.getNode(a.map(Math.round)) })).filter(({ node }) => (fluids.indexOf(node) === -1)).map(({ points, node }) => ({ distance:thag(points.map(Math.round), coords), node })).sort(byDistance).map(({ node }) => node).slice(-1)[0], coords => neighbors(coords,[2,2,2]).map(c=>c[0].map(Math.round)).map(NC.getNode), seed => () => Math.abs((Math.log(seed++) * 10000) % 1), ev => ({ match }) => match.find((matcher) => Object.keys(matcher).reduce((accum, key) => accum && matcher[key] === ev[key], true)), (vl, a) => ((a.velocity) || (a.onceOnly === true && active[a.action] === 'done')) ? ((a.onceOnly ? (active[a.action] = 'done') : true) && { ...vl, vel: vl.vel.map((c,i) => c + (a.velocity[i]*MS.directions[i]*vl.fr)) }) : vl, ({ coords: c }) => boundAngle(Math.atan2(c[1] - VP.coords[1], c[0] - VP.coords[0]) - VP.angle[0]) < Math.PI, a => a < 0 ? ((Math.PI * 2) - (Math.abs(a) % (Math.PI * 2))): (a % (Math.PI * 2)), ({ faces, material }) => faces.map(face => ({ ...face, material, distance: thag(VP.coords, face.center) })).sort(byDistance).slice(-3), () => NC.forRangeProcess(negVS(PL.coords, LD.renderSize).map(Math.round), addVS([0,0,0], LD.renderSize*2).map(Math.round), reloadNode).promise().then(() => Object.keys(viewRange).slice(0,2000).filter(key => (Date.now() - viewRange[key].time) > LD.renderTimeout).forEach(key => delete viewRange[key])), event => ((canvas.width = window.innerWidth) && (canvas.height = window.innerHeight) && (VP.size = [canvas.width, canvas.height])), (center, size = [1,1,1]) => neighbors(center, size).map(([cent,m,i]) => ({ center: cent, coords: facesOrder(comb({ s: setV([2,2,2],i,1), c: setV(center, i, m===1 ? center[i]+size[i] : center[i]), m: size })) })), (center, size = [1,1,1]) => [].concat(...[0,1,2].map(i => [1,-1].map(m => [ setV(center,i,center[i]+(size[i]/2*m)), m, i ]))) ]
const rotate = (coords, center, angles) => { // rotate 3D coords by the angles about the center
	const [dx,dy,dz] = [ coords[0] - center[0], coords[1] - center[1], coords[2] - center[2] ]
	const [sinA, cosA, sinB, cosB] = [ Math.sin(angles[0]), Math.cos(angles[0]), Math.sin(angles[1]), Math.cos(angles[1]) ]
	const y2 = dy * cosA - dx * sinA
	return	[ center[0] + (dx * cosA + dy * sinA), center[1] + (y2 * cosB - dz * sinB), center[2] + (dz * cosB + y2 * sinB) ]
}
const rotateI = (coords, center, angle) => rotate(rotate(coords, center, [0,-angle[1]]), center, [-angle[0],0])
const project = ({ coords = [0,0,0], angle = [0,0], perspective = 1000, size = [100,100] }, pointCoords) => { // projects 3D coords into 2D coords given a view position & angle
	const rotated = rotate(pointCoords, coords, angle)
	const perspectiveDistance = perspective / (rotated[1] - coords[1])
	return perspectiveDistance < 0 ? false : [ ((rotated[0] - coords[0]) * perspectiveDistance) + (size[0] / 2), -((rotated[2] - coords[2]) * perspectiveDistance) + (size[1] / 2) ]
}
const drawPolygon = (context, points) => { // draws a closed polygon
	if (points.length < 3) return // polygons must have at least 3 points
	const [first, ...otherPoints] = points
	context.beginPath()
	context.moveTo(first[0], first[1])
	otherPoints.forEach(point => context.lineTo(point[0], point[1]))
	context.closePath()
	context.fill()
	context.stroke()
}
function comb({ d = 3, s = 2, m = 1, c = 0, centered = true }) {
	const [ ss, ms, cs ] = [ s instanceof Array ? s : Array(d).fill(s), m instanceof Array ? m : Array(d).fill(m), c instanceof Array ? c : Array(d).fill(c) ]
	return Array(ss.reduce((a,b)=>a*b,1)).fill(c).map((J,b) => Array(d).fill(c).reduce(({b,r}, c,i) => {
		const csi = cs[i]-(centered?ms[i]/2:0)
		if (b === 0) return { b, r:r.concat(csi) }
		const mag = ss.slice(i+1).reduce((a,b) => a*b,1)
		if (mag > b) return { b, r:r.concat(csi) }
		const e = Math.floor(b/mag)
		return {b:b-(e*mag),r:r.concat([csi+(ms[i]*e)])}
	},{b,r:[]}).r)
}
function Process(processor, initial = [], config = {}) {
	const { take = 1, delay = 0 } = config
	let [ resolve, reject, activeDelay, processed ] = [ false, false, false, 0 ]
	const [ promise, step, stepWise ] = [ new Promise((res, rej) => ((resolve = res) && (reject = rej))), data => processor(data, { statistics: { processed }}), () => {
		const [ t1, taken ] = [ Date.now(), queue.splice(0,take) ]
		return taken.forEach(t => step(t) || (processed += 1)) || (queue.length ? (activeDelay = setTimeout(stepWise, delay+Date.now()-t1)) : resolve({ processed }))
	} ]
	const queue = [].concat(initial)
	activeDelay = setTimeout(stepWise, delay)
	return { promise: () => promise }
}
function chunkLoader ({ chunkSize = 32, loadMarker = undefined, rangeMarker = null, processConfig = {take:1000,delay:0,autostart:true},  fieldRange = [Number.MAX_SAFE_INTEGER+1,Number.MAX_SAFE_INTEGER+1,Number.MAX_SAFE_INTEGER+1], generator = coords => coords[coords.length-1] === 0 ? 1 : 0 }) {
	const [ loadedData, loadedChunks, getIndex, inrangeF, boundF, loadChunksNear, setNode ] = [ {}, [], coords => coords.map(n => n.toString(36)).join(','), range => coords => !coords.find((c, i) => c < 0 || c >= range[i]), range => (coords, offset = 0) => coords.map((c, i) => c < 0 ? 0 : (c > (range[i] - offset) ? (range[i] - offset) : c)), (position, loadRange = thag([0,0,0], chunkSizes)) => comb({ d:fieldRange.length, s:3, centered: false, c:-1 }).map(c => ({ coords: addV(chunkBound(divV(position, chunkSizes).map(Math.floor),1), c), distance: thag(multV(addV(chunkBound(divV(position, chunkSizes).map(Math.floor),1), addVS(c, 0.5)), chunkSizes), position) })).filter(({ distance, coords }) => (distance < loadRange) && chunkInrange(coords)).sort(byDistance).filter(({ coords, cacheIndex }) => (cacheIndex = getIndex(coords)) && loadedChunks.indexOf(cacheIndex) === -1 && (loadedChunks.push(cacheIndex) !== -1)).map(({ coords }) => forRangeProcess(multV(coords, chunkSizes), chunkSizes, (pcoords, index) => index in loadedData ? false : (loadedData[index] = generator(pcoords, index)))), (coords, value) => inrange(coords) ? loadedData[getIndex(coords)] = value : rangeMarker ]
	const [ bound, inrange ] = [ boundF(fieldRange), inrangeF(fieldRange) ]
	const chunkSizes = bound(chunkSize instanceof Array ? chunkSize : fieldRange.map(() => chunkSize))
	const [ chunkBound, chunkInrange ] = [ boundF(divV(fieldRange, chunkSizes).map(Math.floor)), inrangeF(divV(fieldRange, chunkSizes).map(Math.floor)) ]
	const getNode = coords => { // get node at coords right now, or falsey
		if (!inrange(coords)) return rangeMarker
		const index = getIndex(coords)
		return (index in loadedData) ? loadedData[index] : loadMarker
	}
	const forRangeProcess = (USloadCoords, USloadSize, funct, processOpts = processConfig) => {
		const [ loadCoords, loadSize ] = [ bound(USloadCoords), bound(USloadSize) ]
		const forIndexToCoords = index => loadSize.map((c,i) => Math.floor(index / (Array(i).fill(0).map((a,j) => loadSize[j]).reduce((a,b) => a*b, 1))) % c) // TODO: this could be re-written to load closest first?
		return new Process((item, { statistics }) => {
			const coords = addV(forIndexToCoords(statistics.processed), loadCoords)
			funct(coords, inrange(coords) ? getIndex(coords) : -1)
		}, Array(loadSize.reduce((a,b) => a*b, 1)).fill(loadMarker), processOpts) // TODO: avoid filling array?
	}
	return { bound, getNode, setNode, getIndex, loadChunksNear, forRangeProcess }
}
const drawFace = ({ points, color }) => {
	context.strokeStyle = (VP.borders ? (context.fillStyle = color) && 'black' : (context.fillStyle = color))
	drawPolygon(context, points)
}
const getFace = ({ material, distance, coords }) => {
	const points = coords.map(c => project(VP, c)).filter(notFalsey)
	if (points.length < 3) return false
	const bounds = getBounds(points)
	return (bounds[0][1] < 0 || bounds[1][1] < 0 || bounds[0][0] > VP.size[0] || bounds[1][0] > VP.size[1]) ? false : { points, color: getColor(material, distance) }
}
const getBounds = polygon => [0,1].map(xy => {
	const c = polygon.map(p => p[xy])
	return [Math.min(...c),Math.max(...c)]
})
const drawFrame = () => {
	const polygons = [].concat(...Object.values(viewRange).filter(behindYou).map(filterBlockPolygons), ...Object.values(customPolygons)).sort(byDistance).map(getFace).filter(notFalsey).slice(-1000)
	context.clearRect(0, 0, canvas.width, canvas.height)
	polygons.forEach(drawFace)
	if (animating) window.requestAnimationFrame(drawFrame)
}
const reloadNode = (coords, index) => {
	if (index in viewRange) return viewRange[index].time = Date.now()
	const material = NC.getNode(coords)
	if (material < 1 || isUndefined(material)) return
	const faces = getFaces(coords)
	if (faces) viewRange[index] = { material, coords, faces, time:Date.now() }
}
const getFaces = coords => {
	const index = NC.getIndex(coords)
	const knowNode = knownFaces[index]
	if (knowNode === false || knowNode instanceof Array) return knowNode
	const neighbors = getCubeNeighbors(coords)
	const [ pre, faces ] = [ neighbors.find(isUndefined), knowNode instanceof Object ? knowNode.faces : getCubeFaces(coords) ]
	const result = faces.filter((p, i) => neighbors[i] === 0 || isUndefined(neighbors[i]))
	return pre ? (knowNode instanceof Object ? (result.length ? result : false) : (knownFaces[index] = { faces })) : (knownFaces[index] = result.length ? result : false)
}
const move = direction => {
	const newPosition = addV(PL.coords, direction)
	return collides(newPosition,multV(PL.size, [1,1,0.5])) === false && collides(addV(newPosition, [0,0,-1]), multV(PL.size, [1,1,0.5])) === false
}
let running = false
const run = () => {
	running = setTimeout(run, LD.runInt)
	const [ actives, below ] = [ Object.values(active).filter(notFalsey), getNearest(addV(PL.coords, [0,0,-(PL.size[2]-0.5)])) ]
	const floorFriction = below > 0 ? frictions[below] : frictions[0]
	const rotated = rotateI(actives.reduce(mergeActive, {fr: 1 - floorFriction, vel:[0,0,0]}).vel, [0, 0, 0], [PL.angle[0], 0])
	MS.velocity = addV(MS.velocity, [0,0,-MS.gravity]).map((c,i) => {
		const [d,v] = [(c+rotated[i])*((i !== 2)?floorFriction:frictions[0]), [0, 0, 0]]
		return (v[i] = d) && (move(v) ? ((PL.coords = addV(PL.coords, v)) && d) : 0)
	})
	if (PL.coords[2] < -10) PL.coords = addV(multV(NC.bound(PL.coords,1),[1,1,0]),addV(multV(LD.gameSize,[0,0,1]), [0,0,PL.size[2]])) // if the player falls out of bounds, put them back in
	const _ = VP.mode === 3 ? ((VP.angle = [boundAngle(PL.angle[0]+Math.PI), -PL.angle[1]]) && (VP.coords = addV(PL.coords, rotateI([0,10,0], [0,0,0], PL.angle))) && (customPolygons.PL = filterBlockPolygons({ material: '#ff8855', faces: getCubeFaces(addV(PL.coords,[0,0,-0.5]), PL.size) }))) : ((VP.coords = PL.coords) && (VP.angle = PL.angle) && (customPolygons.PL ? delete customPolygons.PL : true))
	PL.holding ? (customPolygons.holding = filterBlockPolygons({ material: PL.holding, faces: getCubeFaces(addV(PL.coords,rotateI([Math.min(0.5, VP.size[0]/2000),1,-0.2],[0,0,0],PL.angle)), [0.2,0.2,0.2]) })) : (customPolygons.holding ? delete customPolygons.holding : true)
	if ((active.dig && active.dig !== 'done') || (active.place && active.place !== 'done')) {
		if (active.dig) active.dig = 'done'
		if (active.place) active.place = 'done'
		const rangeEnd = rotateI([0, PL.actRange, 0], [0, 0, 0], PL.angle)
		const [ gradient, signs ] = [ multVS(rangeEnd, 1/PL.actRange), rangeEnd.map(Math.sign) ]
		const [ bsd, bsp, factors ] = [ [], [], [].concat(...rangeEnd.map((c,j) => Array(Math.floor(Math.abs(c))).fill(0).map((c,i) => ({ face: j, dist: ((Math.round(PL.coords[j] + i*signs[j])+(0.5*signs[j]))-PL.coords[j])/gradient[j] })))).filter(a => a.dist >= 0 && a.dist <= PL.actRange).sort((a,b) => a.dist-b.dist) ]
		factors.forEach(factor => {
			const [ pn, d ] = [ addV(PL.coords, multVS(gradient, factor.dist)), setV([0,0,0], factor.face, 0.5*signs[factor.face]) ]
			return ((bsd.push(addV(pn, d).map(Math.round)) !== -1) && (active.place ? bsp.push(addV(pn, multVS(d,-1)).map(Math.round)) : false))
		})
		const delI = bsd.findIndex(coords => fluids.indexOf(NC.getNode(coords)) === -1)
		if (delI !== -1) return (active.dig ? placeBlock(bsd[delI], 0) : ((active.place && PL.holding) ? placeBlock(bsp[delI], PL.holding) : false))
	}
}
const [ start, stop ] = [ () => (intervals.chunks = setInterval(() => NC.loadChunksNear(PL.coords, LD.renderSize*2), LD.chunkInt)) && (intervals.view = setInterval(reloadView, LD.redrawInt)) && (running = setTimeout(run, LD.runInt)) &&	window.requestAnimationFrame(drawFrame) && (animating = true), () => clearTimeout(running) || (running = false) || (animating = false) || (Object.values(intervals).map(clearInterval) && false) ]
const urlParams = new URLSearchParams(window.location.search)
const seed = urlParams.has('seed') ? parseFloat(urlParams.get('seed')) : Math.random()
const [ random, newFourierTerm, newFourierSeries, newFourierConfig ] = [ randomGenerator(seed), (funct, [m = 1, n = 1, c = 0, d = 0]) => x => c + (m * funct(n * (x + d))), terms => x => terms.reduce((acc, term) => acc + term(x), 0), terms => ['x', 'y'].map(() => Array(terms).fill(0).map(() => [random(), random(), (random() - 0.5) * 2, (random() - 0.5) * 2])) ]
const terrainFunctions = terrains.map(c => c ? (((D2conf) => ([x,y]) => D2conf[0](x)+D2conf[1](y))(newFourierConfig(c).map(cnf => newFourierSeries(cnf.map(n => newFourierTerm(Math.sin, n)))))) : () => 0)
const NC = chunkLoader({ fieldRange: LD.gameSize, chunkSize: LD.chunkSize, generator: (coords, index) => (index in storedBlocks) ? parseInt(storedBlocks[index]) : (coords[2] === 0 ? 1 : Math.max(terrainFunctions.findIndex(f => f(coords) > coords[2]), 0))})
const placeBlock = (coords, material = 0) => {
	const originalMaterial = NC.getNode(coords)
	NC.setNode(coords, material)
	if (fluids.indexOf(material) === -1 && !move([0,0,0])) return NC.setNode(coords, originalMaterial)
	const index = NC.getIndex(coords)
	const _ = (localStorage[storageKey] = localStorage[storageKey]+"|"+([material, ...coords].join(','))) && (storedBlocks[index] = material)
	const refreshes = [[coords, index]].concat(neighbors(coords,[2,2,2]).map(c=>c[0].map(Math.round)).map(coords => [coords, NC.getIndex(coords)])).filter(([c,i]) => ((i in knownFaces) ? delete knownFaces[i] : true) && ((i in viewRange) ? delete viewRange[i] : true)).forEach(([c,i]) => reloadNode(c,i))
}
const storageKey = `minutecraft-custom-${seed}`
if (!(storageKey in localStorage)) localStorage[storageKey] = ""
localStorage[storageKey].split('|').filter(str => str.length).map(str => str.split(',').map(a => parseInt(a))).forEach(([material, ...coords]) => storedBlocks[NC.getIndex(coords)] = material)
localStorage[storageKey] = "|"+(Object.entries(storedBlocks).map(([index,material]) => ([material, ...index.split(',').map(a => parseInt(a, 36))].join(','))).join('|'))
const lockPointerMouseMove = (element, callback) => {
	const [ lockChangeAlert, bindAnyway ] = [ () => (document.pointerLockElement === element || document.mozPointerLockElement === element) ? document.addEventListener("mousemove", callback, false) : document.removeEventListener("mousemove", callback, false), () => document.addEventListener("mousemove", callback, false) ]
	return (element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock) && (document.exitPointerLock = document.exitPointerLock || document.mozExitPointerLock) && (element.onclick = () => element.requestPointerLock()) && (document.addEventListener('pointerlockerror', bindAnyway, false) ||	document.addEventListener('mozpointerlockerror', bindAnyway, false) || document.addEventListener('pointerlockchange', lockChangeAlert, false) || document.addEventListener('mozpointerlockchange', lockChangeAlert, false))
}
const evDown = event => { // register action as active on press
	const action = actions.find(findAction(event))
	if (action && !active[action.action]) active[action.action] = action
}
const evUp = event => { // unregister action as active on release
	const action = actions.find(findAction(event))
	if (action) active[action.action] = false
}
PL.coords = addV(multV(LD.gameSize, [0.5,0.5,1]), [0,0,PL.size[2]])
document.write(`<i style="text-align:center;width:100%;height:100px;line-height:100px;position:absolute;top:0;bottom:0;left:0;margin:auto;">loading...</i>`)
Promise.all(NC.loadChunksNear(PL.coords, LD.renderSize).map(l => l.promise())).then(reloadView).then(() => {
	(document.body.innerHTML = "<style>*{padding:0;margin:0;background:#8af;}</style><canvas></canvas>") && (window.canvas = document.getElementsByTagName('canvas')[0]) && (window.context = canvas.getContext('2d'))
	canvasSize()
	const _ = (window.addEventListener("resize", canvasSize) || document.addEventListener("keydown", evDown) || document.addEventListener("keyup", evUp) || document.addEventListener("mousedown", evDown) || document.addEventListener("mouseup", evUp))
	document.addEventListener("contextmenu", event => {
		if(event.preventDefault != undefined) event.preventDefault()
		if(event.stopPropagation != undefined) event.stopPropagation()
		return false
	})
	document.addEventListener("keydown", event => {
		const ki = parseInt(event.key)
		if (event.key == ki) PL.holding = ki < colors.length ? ki : 0
	})
	lockPointerMouseMove(canvas, event => (PL.angle[0] -= (event.movementX * MS.turn)) && (PL.angle[1] += (event.movementY * MS.turn)) && (PL.angle[0] = boundAngle(PL.angle[0])) && ((PL.angle[1] > VP.limit) ? (PL.angle[1] = VP.limit) : true) && ((PL.angle[1] < -VP.limit) ? (PL.angle[1] = -VP.limit) : true)) // Look around with the cursor
	start()
})
// How much space do I have left?
